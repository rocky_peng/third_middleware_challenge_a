import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Author :  Rocky
 * Date : 25/05/2017 17:44
 * Description :
 * Test :
 */
public class NotBlockingQueueTest<T> {

    public static void main(String[] args) {

        NotBlockingQueue<Object> queue = new NotBlockingQueue<>(300);
        Thread takeThread = null;
        final int[] takeCount = {0};

        AtomicInteger putCount = new AtomicInteger(0);
        for (int i = 0; i < 1; i++) {
            takeThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            Object o = queue.take();
                            while (o == null) {
                                try {
                                    Thread.sleep(10);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                o = queue.take();
                            }
                            takeCount[0]++;
                        }
                    } catch (Exception e) {
                        System.out.println("take线程 " + Thread.currentThread() + " 停止");
                    }
                }
            });
            takeThread.start();
        }

        List<Thread> putThreads = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            queue.put(new Object());
                            try {
                                Thread.sleep(1);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            putCount.incrementAndGet();
                        }
                    } catch (Exception e) {
                        System.out.println("put线程 " + Thread.currentThread() + " 停止");
                    }
                }
            });
            putThreads.add(t);
            t.start();
        }

        Scanner scanner = new Scanner(System.in);
        try {
            String command = scanner.next();
            if (command != null && command.length() > 0) {
                for (Thread putThread : putThreads) {
                    putThread.interrupt();
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {

                }
                takeThread.interrupt();
            }

            System.out.println(queue.innerQueue.size());
            System.out.println("putCount  " + putCount.get() + ", takeCount " + takeCount[0]);
        } finally {
            System.exit(-1);
        }
    }

}
