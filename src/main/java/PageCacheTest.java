import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Author :  Rocky
 * Date : 31/05/2017 14:53
 * Description :
 * Test :
 */
public class PageCacheTest {


    public static void main(String[] args) throws IOException, InterruptedException {

        System.out.println("将1024m数据全部读到page cache中");

        long s = System.currentTimeMillis();
        for (int i = 1; i <= 10; i++) {
            RandomAccessFile raf = new RandomAccessFile("/data/pqs/msg." + i, "rw");
            MappedByteBuffer mbb = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, 1024 * 1024 * 1024);
            byte a = 0;
            while (mbb.hasRemaining()) {
                a = mbb.get();
            }
        }

        System.out.println(System.currentTimeMillis() - s);

        System.out.println(" page cache 应该全部有这1024m数据，下面重复读这1024m数据，看dstat监控数据 ");

        Thread.sleep(10000);


        s = System.currentTimeMillis();

        /*for (int i = 0; i < 10; i++) {
            RandomAccessFile raf = new RandomAccessFile("/data/pqs/msg.10", "rw");
            MappedByteBuffer mbb = raf.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, 1024 * 1024 * 1024);
            byte[] data = new byte[4 * 1024];
            while (mbb.hasRemaining()) {
                mbb.get(data);
            }
        }*/


        System.out.println((System.currentTimeMillis() - s));
    }
}
