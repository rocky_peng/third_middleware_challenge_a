import io.openmessaging.KeyValue;
import io.openmessaging.Producer;
import io.openmessaging.demo.DefaultKeyValue;
import io.openmessaging.demo.DefaultProducer;
import io.openmessaging.demo.MiscUtils;
import io.openmessaging.demo.StoreUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Author :  Rocky
 * Date : 20/05/2017 15:25
 * Description :
 * Test :
 */
public class ProducerTest {

    public static void main(String[] args) throws Exception {

        String path = "";
        if (args == null || args.length <= 0 || args[0] == null) {
            path = "/Users/zuji/Documents/project/open-messaging-demo";
        } else {
            path = args[0];
        }
        KeyValue kvs = new DefaultKeyValue();
        kvs.put("STORE_PATH", path);

        CountDownLatch latch = new CountDownLatch(10);

        List<List<String>> topicList = new ArrayList<>(20);
        int k = 0;
        for (int i = 0; i < 10; i++) {
            List<String> topcs = new ArrayList<>(10);
            for (int j = 0; j < 10; j++) {
                k++;
                topcs.add("topic-" + k);
            }
            topicList.add(topcs);
        }

        long s = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            int finalI = i;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    Producer producer = new DefaultProducer(kvs);

                    List<String> topics = topicList.get(finalI);

                    for (int i1 = 0; i1 < topics.size(); i1++) {
                        String topic = topics.get(i1);

                        for (int j = 0; j < 400000; j++) {
                            producer.send(MiscUtils.buildMsg(topic));
                        }
                    }

                    latch.countDown();
                }
            });
            t.start();
        }

        latch.await();
        long e = System.currentTimeMillis();
        System.out.println("发送4kw消息，耗时 " + (e - s) + " ms，总字节 " + StoreUtils.wroteBytes + " byte，速度 " + (StoreUtils.wroteBytes.get() / 1024 / ((e - s) / 1000)) + " kb/s");
    }

//9765kx4qakh0hcgdwdk2xqlfrpvmr0hfzsu70kkqwlqk44o3cg1793licojfhwqa373jz0dlcitqi5e940afkr0mt46wkdj9o44g4u78v29vbyb0rxyq4fflrn5dw6cxa9z3btj3xrcxz8ibaj8g7jxp2zn2p7tlvhq2cq4t5kk2q5js16xqaxiqplyg3x3bw4ibbg7v48htk5utfdlburrtslw92fpwkpn0ovi4tx1axer6gqkih52uwnynatbf0i4zvkzd1xmm5vnwdv9pp2lpxyv9fod1vp7zwmxekd5no1hz2q188y0hic9rxsoan3ep3lt385avnaebu581sel3442dpvfpc9xv0yuvo95cw69zqlmd0h6i2lz7dg7bf9yyn1kf6hyckz2clyjlrgmnkhomikuc7cozg9ziqlcy0sll34ikbiiaoitw51q3dquhou8xt5w7mq676js1bj5y799voyy5xilcx0tcj9u9bv2uf3pqb43l0ep2o0nuyah8cw973k1uwnwa9bg3ysokh5h52eb99txpl6pthsnsz91aydzyagkqiep1ctsbdeq0xpbambeh6tv3uro9ifsl0lt8sgawin53qt68uavwnkk0mvo26lfyiusskf3blyvw6em5g6pd4xriljr3mju81k3zmjlxykn2jhwq9h65f8dxssp7yw8w5xejovrh82k9aaqyg8giqy54zv5am95y5jp7ksax8uxdww3a2f02jb7o5609m9l19kzgp1wuttrf73wyu9ucw5z1q9aa6ssemahouqzzwb6ptgf91ky30ie3kynn2vv1se3s54fy9n1hpa7sbzbaqebpcwuiepltus9hd2isj3a2qxsvq1a2kkmv0fshqznrdgjeh5ypq63gieixxorw1wfb46t7yofzwi8b74qgv3hzg0ezueqtpvnnbrtxmtj0w6l824rixcbbzcvwhkjcs4l76w0d7tnn00mti3v6pnnhttaac
}