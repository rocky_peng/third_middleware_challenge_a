package io.openmessaging.demo;

import io.openmessaging.BytesMessage;
import io.openmessaging.KeyValue;
import io.openmessaging.Message;

import java.util.concurrent.atomic.AtomicLong;

public class DefaultBytesMessage implements BytesMessage {

    private KeyValue headers = new DefaultKeyValue();
    private KeyValue properties = new DefaultKeyValue();
    private byte[] body;

    private static final AtomicLong msgCount = new AtomicLong(0);

    public DefaultBytesMessage(byte[] body) {
        this.body = body;
    }

    public DefaultBytesMessage() {
    }

    @Override
    public byte[] getBody() {
        return body;
    }

    @Override
    public BytesMessage setBody(byte[] body) {
        this.body = body;
        return this;
    }

    @Override
    public KeyValue headers() {
        return headers;
    }

    @Override
    public KeyValue properties() {
        return properties;
    }

    @Override
    public Message putHeaders(String key, int value) {
        headers.put(key, value);
        MiscUtils.printNewHeader(key, value);
        return this;
    }

    @Override
    public Message putHeaders(String key, long value) {
        headers.put(key, value);
        MiscUtils.printNewHeader(key, value);
        return this;
    }

    @Override
    public Message putHeaders(String key, double value) {
        headers.put(key, value);
        MiscUtils.printNewHeader(key, value);
        return this;
    }

    @Override
    public Message putHeaders(String key, String value) {
        headers.put(key, value);
        MiscUtils.printNewHeader(key, value);
        return this;
    }

    @Override
    public Message putProperties(String key, int value) {
        /*if (properties == null) {
            properties = new DefaultKeyValue();
        }*/
        properties.put(key, value);
        MiscUtils.printNewProperty(key, value);
        return this;
    }

    @Override
    public Message putProperties(String key, long value) {
//        if (properties == null) {
//            properties = new DefaultKeyValue();
//        }
        properties.put(key, value);
        MiscUtils.printNewProperty(key, value);
        return this;
    }

    @Override
    public Message putProperties(String key, double value) {
//        if (properties == null) {
//            properties = new DefaultKeyValue();
//        }
        properties.put(key, value);
        MiscUtils.printNewProperty(key, value);
        return this;
    }

    @Override
    public Message putProperties(String key, String value) {
//        if (properties == null) {
//            properties = new DefaultKeyValue();
//        }
        properties.put(key, value);
        MiscUtils.printNewProperty(key, value);
        return this;
    }

}
