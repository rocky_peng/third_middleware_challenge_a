package io.openmessaging.demo;

import io.openmessaging.BytesMessage;
import io.openmessaging.KeyValue;
import io.openmessaging.Message;
import io.openmessaging.MessageHeader;

import java.util.Set;

/**
 * Author :  Rocky
 * Date : 20/05/2017 10:17
 * Description :
 * Test :
 */
public class MiscUtils {

    public static void printKVS(String desc, KeyValue kvs) {
        if (kvs == null) {
            return;
        }

        System.out.println(desc);
        for (String key : kvs.keySet()) {
            Object value;
            value = getValue(kvs, key);
            System.out.println(key + " : " + value);
        }
        System.out.println("==========================");
    }

    public static Object getValue(KeyValue kvs, String key) {
        Object value;
        try {
            value = kvs.getInt(key);
        } catch (Exception e) {
            try {
                value = kvs.getLong(key);
            } catch (Exception ex) {
                try {
                    value = kvs.getDouble(key);
                } catch (Exception exx) {
                    value = kvs.getString(key);
                }
            }
        }
        return value;
    }


    public static void printNewHeader(String key, Object value) {
//        System.out.println("putHeaders[key=" + key + ",value=" + value + ",value_type=" + value.getClass().getSimpleName() + "]");
    }

    public static void printNewProperty(String key, Object value) {
//        System.out.println("putProperties[key=" + key + ",value=" + value + ",value_type=" + value.getClass().getSimpleName() + "]");
    }


    public static BytesMessage buildMsg() {
        BytesMessage msg = new DefaultBytesMessage();
        msg.putHeaders(MessageHeader.SEARCH_KEY, 1);
        msg.putHeaders(MessageHeader.SHARDING_KEY, 2l);
        msg.putHeaders(MessageHeader.MESSAGE_ID, 3.0);
        msg.putHeaders(MessageHeader.TIMEOUT, "4");
        msg.putProperties("5", 5);
        msg.putProperties("6", 6l);
        msg.putProperties("7", 7.0);
        msg.putProperties("8", "8");
        msg.setBody("I LOVE YOU".getBytes());
        return msg;
    }


    public static BytesMessage buildMsg(String topic) {
        BytesMessage message = buildMsg();
        message.headers().put("Topic", topic);
        return message;
    }


    public static boolean testEquals(Message oldMsg, Message newMsg) {
        if (oldMsg == null && newMsg == null) {
            return true;
        }
        if (oldMsg != null && newMsg != null) {
            boolean result = testKeyValue(oldMsg.headers(), newMsg.headers())
                    && testKeyValue(oldMsg.properties(), newMsg.properties());
            if (result) {
                if (oldMsg instanceof BytesMessage && newMsg instanceof BytesMessage) {
                    return testBody(((BytesMessage) oldMsg).getBody(), ((BytesMessage) newMsg).getBody());
                }
            }
        }
        return false;
    }

    public static boolean testBody(byte[] body, byte[] body1) {
        if (body == null && body1 == null) {
            return true;
        }
        if (body != null && body1 != null) {
            return new String(body).equals(new String(body1));
        }
        return false;
    }

    public static boolean testKeyValue(KeyValue headers, KeyValue headers1) {
        if (headers == null && headers1 == null) {
            return true;
        }
        if (headers != null && headers1 != null) {
            Set<String> set = headers.keySet();
            Set<String> set1 = headers1.keySet();
            if (set.size() != set1.size()) {
                return false;
            }
            for (String key : set) {
                Object value = MiscUtils.getValue(headers, key);
                Object value1 = MiscUtils.getValue(headers1, key);
                if (!value.equals(value1)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }


}
